# FFmpeg for Android

Repozitář obsahuje skript pro sestavení knihovny FFmpeg pro operační systém Android.

Při spouštění skriptu je třeba mít zavedenou proměnnou ANDROID_PLATFORM, která představuje
verzi platformy.

V záhlaví skriptu je třeba nastavi umístění SDK a NDK. Úspěšně je otestováno NDK verze 14b.

Skript očekává jeden parametr a tím je cílové ABI (armeabi-v7a | x86).

Zdroje budou staženy automaticky a po sestavení binárních souborů budou automaticky odstraněny.
Vytvořené binární soubory knihoven budou na konci sestavování uloženy do adresáře build/ffmpeg/android-$ABI.
