#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Chyba: Skript očekává 1 parametr -- ABI pro které má být knihovna sestavena (armeabi-v7a | x86)."
    exit 1
fi

if [ -z "$ANDROID_PLATFORM" ]; then
    echo "error: undefined NDK_PLATFORM_LEVEL"
    exit 1
fi

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

################################################################
### Konfigurační konstanty #####################################
################################################################
NDK_PATH="$HOME/Programming/AndroidNDK/android-ndk-r14b"
ANDROID_SDK="$HOME/Programming/AndroidSDK"

# Adresář, kam se budou stahovat zdrojové soubory knihovny.
SOURCE_DIR=$CURRENT_DIR/sources

# Adresář, kam se uloží sestavené knihovny.
BUILD_DIR=$CURRENT_DIR/build/ffmpeg/android-$1

################################################################
### Příprava sestavení knihovny ################################
################################################################

if [ $1 == "armeabi-v7a" ]; then
    CROSS_PREFIX=$NDK_PATH/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin/arm-linux-androideabi-
    SYSROOT=$NDK_PATH/platforms/android-$ANDROID_PLATFORM/arch-arm
    EXTRA_CFLAGS="-march=armv7-a -mfloat-abi=softfp -mfpu=vfpv3-d16"
    EXTRA_LDFLAGS="-march=armv7-a -Wl,--fix-cortex-a8"
    CPU=armv7-a
    ARCH=armv7-a
elif [ $1 == "x86" ]; then
    CROSS_PREFIX=$NDK_PATH/toolchains/x86-4.9/prebuilt/linux-x86_64/bin/i686-linux-android-
    SYSROOT=$NDK_PATH/platforms/android-$ANDROID_PLATFORM/arch-x86
    EXTRA_CFLAGS="-pipe -march=atom -msse3 -ffast-math -mfpmath=sse"
    EXTRA_LDFLAGS="-lm -lz -Wl,--no-undefined -Wl,-z,noexecstack"
    CPU=i686
    ARCH=x86
else
    echo "Chyba: Nepodporované ABI ($1)."
    exit 1
fi

# Odstraňování přechozích sestavení.
echo "Odstraňuji všechny binární soubory..."
rm -rf $BUILD_DIR


################################################################
### Sestavování knihovny #######################################
################################################################
mkdir -p $SOURCE_DIR
cd $SOURCE_DIR

if [ ! -d "FFmpeg" ]; then
  git clone https://github.com/FFmpeg/FFmpeg.git
fi

cd FFmpeg
git reset --hard b910b34926657531d84269bd7c61fb8c74e5d905
git apply ../../ffmpeg-patch.diff

make distclean > /dev/null 2>&1

./configure \
--prefix=$BUILD_DIR \
--enable-avresample \
--enable-shared \
--disable-debug \
--disable-stripping \
--disable-static \
--disable-ffmpeg \
--disable-ffplay \
--disable-ffprobe \
--disable-ffserver \
--disable-avdevice \
--disable-doc \
--disable-symver \
--cross-prefix=$CROSS_PREFIX \
--target-os=linux \
--enable-cross-compile \
--sysroot=$SYSROOT \
--enable-pic \
--extra-cflags="-fpic $EXTRA_CFLAGS" \
--extra-ldflags="$EXTRA_LDFLAGS" \
--cpu=$CPU \
--arch=$ARCH

make clean
make -j $(nproc --all)
make install

rm -rf $SOURCE_DIR